package xyz.pklite.patchcreator.asm;

public enum TYPES
{
	
	VOID("V"),
	BOOLEAN("Z"),
	CHAR("C"),
	BYTE("B"),
	SHORT("S"),
	INT("I"),
	FLOAT("F"),
	LONG("J"),
	DOUBLE("D");
	
	String sig;
	
	TYPES(String s)
	{
		this.sig = s;
	}
	
	public String getSig()
	{
		return sig;
	}
	
	public String toString()
	{
		return getSig();
	}
	
	public static TYPES fromSig(String si)
	{
		for (TYPES type : TYPES.values())
		{
			if (type.sig.equals(si))
			{
				return type;
			}
		}
		return null;
	}
}
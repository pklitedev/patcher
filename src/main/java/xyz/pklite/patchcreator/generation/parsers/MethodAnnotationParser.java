package xyz.pklite.patchcreator.generation.parsers;

import javassist.CtClass;
import javassist.CtMethod;
import xyz.pklite.patchcreator.annotations.*;
import xyz.pklite.patchcreator.enums.InjectionType;
import xyz.pklite.patchcreator.utils.RefUtils;

import static xyz.pklite.patchcreator.PatchCreator.isMethodTagged;
import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class MethodAnnotationParser
{
	
	private final CtClass patch;
	
	public MethodAnnotationParser(CtClass patch)
	{
		this.patch = patch;
	}
	
	public void run() throws ClassNotFoundException
	{
		for(CtMethod method : patch.getDeclaredMethods())
		{
			Object[] annotations = method.getAnnotations();
			
			boolean reobfuscate = false;
			InjectionType type = null;
			
			for (Object obj : annotations)
			{
				reobfuscate = obj instanceof Reobfuscate || reobfuscate;
				if (obj instanceof Inject)
				{
					type = InjectionType.INJECT;
				}
				else if (obj instanceof Append)
				{
					type = InjectionType.APPEND;
				}
				else if (obj instanceof Overwrite)
				{
					type = InjectionType.OVERWRITE;
				}
				else if (obj instanceof Prepend)
				{
					type = InjectionType.PREPEND;
				}
				else if (obj instanceof Provided)
				{
					type = InjectionType.PROVIDED;
				}
			}
			
			
			
			if (type == null)
			{
				throw new RuntimeException(
						method.getDeclaringClass().getSimpleName() + "." + method.getName() + " is unannotated" +
								"!");
			}
			
			String methodName = method.getName();
			if (type == InjectionType.PREPEND)
			{
				if (!methodName.startsWith("prepend$"))
				{
					throw new RuntimeException(
							method.getDeclaringClass().getSimpleName() + "." + method.getName() + " has a @Prepend " +
									"annotation without beginning with \"prepend$\"!");
				}
			}
			else if (type == InjectionType.APPEND)
			{
				if (!methodName.startsWith("append$"))
				{
					throw new RuntimeException(
							method.getDeclaringClass().getSimpleName() + "." + method.getName() + " has a @Append " +
									"annotation without beginning with \"append$\"!");
				}
			}
			
			if(reobfuscate)
			{
				String r = String.format("%s %s %s", method.getDeclaringClass().getSimpleName(), methodName,
						RefUtils.reobMethodDescriptor(method.getSignature()));
				isMethodTagged.put(r, true);
			}
			
		}
	}
	
}

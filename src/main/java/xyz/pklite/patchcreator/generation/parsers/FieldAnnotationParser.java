package xyz.pklite.patchcreator.generation.parsers;

import javassist.CtClass;
import javassist.CtField;
import xyz.pklite.patchcreator.annotations.Append;
import xyz.pklite.patchcreator.annotations.Inject;
import xyz.pklite.patchcreator.annotations.Overwrite;
import xyz.pklite.patchcreator.annotations.Prepend;
import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;
import xyz.pklite.patchcreator.enums.InjectionType;
import xyz.pklite.patchcreator.utils.RefUtils;

import static xyz.pklite.patchcreator.PatchCreator.isFieldTagged;
import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class FieldAnnotationParser
{
	
	private CtClass clazz;
	
	public FieldAnnotationParser(CtClass clazz)
	{
		this.clazz = clazz;
	}
	
	public void run() throws ClassNotFoundException
	{
		for (CtField field : clazz.getDeclaredFields())
		{
			Object[] annotations = field.getAnnotations();
			
			InjectionType type = null;
			boolean reobfuscate = false;
			
			for (Object obj : annotations)
			{
				reobfuscate = obj instanceof Reobfuscate || reobfuscate;
				if (obj instanceof Inject)
				{
					type = InjectionType.INJECT;
				}
				else if (obj instanceof Append)
				{
					type = InjectionType.APPEND;
				}
				else if (obj instanceof Overwrite)
				{
					type = InjectionType.OVERWRITE;
				}
				else if (obj instanceof Prepend)
				{
					type = InjectionType.PREPEND;
				}
				else if (obj instanceof Provided)
				{
					type = InjectionType.PROVIDED;
				}
			}
			
			String fieldOwner = field.getDeclaringClass().getSimpleName();
			String fieldName = field.getName();
			String fieldDescriptor = RefUtils.reobDescriptor(field.getSignature());
			
			if (type == null)
			{
				throw new RuntimeException(
						field.getDeclaringClass().getSimpleName() + "." + field.getName() + " is unannotated" +
								"!");
			}
			
			if (type != InjectionType.PROVIDED && type != InjectionType.INJECT)
			{
				throw new RuntimeException(
						field.getDeclaringClass().getSimpleName() + "." + field.getName() + " has an invalid " +
								"annotation! @" + type);
			}
			
			if (reobfuscate)
			{
				isFieldTagged.put(fieldOwner + " " + fieldName + " " + fieldDescriptor, true);
			}
		}
	}
	
}

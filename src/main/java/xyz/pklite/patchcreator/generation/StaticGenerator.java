package xyz.pklite.patchcreator.generation;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.ClassRemapper;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.tree.ClassNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
import xyz.pklite.patchcreator.PatchCreator;
import xyz.pklite.patchcreator.utils.JavassistUtils;
import xyz.pklite.patchcreator.utils.RefUtils;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class StaticGenerator
{
	
	private ClassLoader classLoader;
	private Method defineClass;
	
	public static HashMap<String, ArrayList<MethodNode>> staticMethods = new HashMap<>();
	public static HashMap<String, ArrayList<FieldNode>> staticFields = new HashMap<>();
	public static Set<String> modifiedClasses = new HashSet<>();
	
	public StaticGenerator(ClassLoader classLoader) throws NoSuchMethodException
	{
		this.classLoader = classLoader;
		defineClass =
				ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class);
		defineClass.setAccessible(true);
	}
	
	public void run() throws NotFoundException, IOException, CannotCompileException
	{
		CtClass clazz = PatchCreator.classPool.get("xyz.pklite.patchcreator.patches."+RefUtils.STATICS_STRING);
		byte[] bytecode = JavassistUtils.getClassBytecode(clazz);
		ClassReader cr = new ClassReader(bytecode);
		
		ClassNode node = new ClassNode();
		cr.accept(node, 0);
		
		for (MethodNode method : node.methods)
		{
			String methodName = method.name;
			method.desc = RefUtils.reobMethodDescriptor(method.desc);
			int access = method.access;
			if ((access & 8) != 8)
			{
				continue;
			}
			String reobbed = RefUtils.reobMethodNameDangerous(methodName, method.desc);
			if(reobbed == null)
			{
				stderr("Failed to reob static method: %s %s", methodName, method.desc);
				continue;
			}
			String[] split = reobbed.split(" ");
			method.name = split[1];
			ArrayList<MethodNode> list = staticMethods.getOrDefault(split[0], new ArrayList<>());
			list.add(method);
			staticMethods.put(split[0], list);
			modifiedClasses.add(split[0]);
		}
		
		for (FieldNode field : node.fields)
		{
			String fieldName = field.name;
			field.desc = RefUtils.reobDescriptor(field.desc);
			int access = field.access;
			if ((access & 8) != 8)
			{
				continue;
			}
			String reobbed = RefUtils.reobFieldNameDangerous(fieldName, field.desc);
			if(reobbed == null)
			{
				stderr("Failed to reob static field: %s %s", fieldName, field.desc);
				continue;
			}
			String[] split = reobbed.split(" ");
			field.name = split[1];
			ArrayList<FieldNode> list = staticFields.getOrDefault(split[0], new ArrayList<>());
			list.add(field);
			staticFields.put(split[0], list);
			modifiedClasses.add(split[0]);
		}
	}
	
	private Class<?> defineClass(String name, byte[] bytes) throws InvocationTargetException, IllegalAccessException
	{
		return (Class<?>) defineClass.invoke(classLoader, name, bytes, 0, bytes.length);
	}
	
}

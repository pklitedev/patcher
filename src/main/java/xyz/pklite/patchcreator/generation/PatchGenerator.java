package xyz.pklite.patchcreator.generation;

import javassist.CannotCompileException;
import javassist.CtClass;
import xyz.pklite.patchcreator.PatchCreator;
import xyz.pklite.patchcreator.generation.parsers.FieldAnnotationParser;
import xyz.pklite.patchcreator.generation.parsers.MethodAnnotationParser;
import xyz.pklite.patchcreator.generation.transformers.impl.*;
import xyz.pklite.patchcreator.utils.JavassistUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class PatchGenerator
{
	
	private final String className;
	private final byte[] bytecode;
	
	public PatchGenerator(String className, byte[] bytecode)
	{
		this.className = className;
		this.bytecode = bytecode;
	}
	
	public byte[] run()
	{
		byte[] newCode = bytecode;
		
		newCode = new GetFieldDecoderTransformer(className, newCode).transform();
		// https://asm.ow2.io/javadoc/org/objectweb/asm/commons/Remapper.html
		// https://asm.ow2.io/javadoc/org/objectweb/asm/commons/ClassRemapper.html
		newCode = new AsmStaticUsageTransformer(className, newCode).transform();
		newCode = new AsmNameTransformer(className, newCode).transform();
		newCode = new AnnotationRemoverTransformer(className, newCode).transform();
		
		return newCode;
	}
	
}


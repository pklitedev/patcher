package xyz.pklite.patchcreator.generation;

import javassist.CannotCompileException;
import javassist.CtClass;
import xyz.pklite.patchcreator.PatchCreator;
import xyz.pklite.patchcreator.generation.parsers.FieldAnnotationParser;
import xyz.pklite.patchcreator.generation.parsers.MethodAnnotationParser;
import xyz.pklite.patchcreator.utils.JavassistUtils;

import java.io.ByteArrayInputStream;
import java.io.IOException;

public class AnnotationProcessor
{
	
	private final String className;
	private final byte[] finalCode;
	
	public AnnotationProcessor(String className, byte[] finalCode)
	{
		this.className = className;
		this.finalCode = finalCode;
	}
	
	public byte[] run() throws IOException, CannotCompileException, ClassNotFoundException
	{
		byte[] newCode = finalCode;
		
		CtClass clazz = PatchCreator.classPool.makeClass(new ByteArrayInputStream(newCode));
		new FieldAnnotationParser(clazz).run();
		new MethodAnnotationParser(clazz).run();
		newCode = JavassistUtils.getClassBytecode(clazz);
		
		return newCode;
	}
	
}

package xyz.pklite.patchcreator.generation;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

public class OutputGenerator
{
	
	private final String className;
	private final byte[] bytecode;
	
	public OutputGenerator(String className, byte[] bytecode)
	{
		this.className = className;
		this.bytecode = bytecode;
	}
	
	public byte[] run() throws IOException
	{
		return packRaw(bytecode);
	}
	
	public static byte[] packRaw(byte[] b) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		GZIPOutputStream zos = new GZIPOutputStream(baos);
		zos.write(b);
		zos.close();
		
		return baos.toByteArray();
	}
	
}

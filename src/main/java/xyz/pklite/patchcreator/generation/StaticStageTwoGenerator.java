package xyz.pklite.patchcreator.generation;

import org.objectweb.asm.Attribute;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.tree.AnnotationNode;
import org.objectweb.asm.tree.FieldNode;
import org.objectweb.asm.tree.MethodNode;
import org.objectweb.asm.tree.TypeAnnotationNode;
import xyz.pklite.patchcreator.asm.MethodReflector;

import java.util.HashMap;

import static xyz.pklite.patchcreator.PatchCreator.stderr;
import static xyz.pklite.patchcreator.generation.StaticGenerator.*;

public class StaticStageTwoGenerator implements Opcodes
{
	
	private HashMap<String, byte[]> classes;
	
	public StaticStageTwoGenerator(HashMap<String, byte[]> classes)
	{
		this.classes = classes;
	}
	
	public void run()
	{
		stderr("Stage two");
		for (String className : modifiedClasses)
		{
			stderr("%s", className);
			byte[] targetBytecode = classes.getOrDefault(className, null);
			if (targetBytecode == null)
			{
				//create new class
				ClassWriter cw = new ClassWriter(0);
				FieldVisitor fv;
				MethodVisitor mv;
				
				cw.visit(V1_6, ACC_PUBLIC, className, null, "java/lang/Object", null);
				
				if (staticMethods.get(className) != null)
				{
					for (MethodNode method : staticMethods.get(className))
					{
						mv = cw.visitMethod(method.access, method.name, method.desc, method.signature,
								method.exceptions.toArray(new String[0]));
						MethodReflector reflector = new MethodReflector(mv);
						method.accept(reflector);
					}
				}
				
				if (staticFields.get(className) != null)
				{
					for (FieldNode field : staticFields.get(className))
					{
						fv = cw.visitField(field.access, field.name, field.desc, field.signature, field.value);
						int i;
						int n;
						AnnotationNode annotation;
						if (field.visibleAnnotations != null)
						{
							i = 0;
							
							for (n = field.visibleAnnotations.size(); i < n; ++i)
							{
								annotation = (AnnotationNode) field.visibleAnnotations.get(i);
								annotation.accept(fv.visitAnnotation(annotation.desc, true));
							}
						}
						
						if (field.invisibleAnnotations != null)
						{
							i = 0;
							
							for (n = field.invisibleAnnotations.size(); i < n; ++i)
							{
								annotation = (AnnotationNode) field.invisibleAnnotations.get(i);
								annotation.accept(fv.visitAnnotation(annotation.desc, false));
							}
						}
						
						TypeAnnotationNode typeAnnotation;
						if (field.visibleTypeAnnotations != null)
						{
							i = 0;
							
							for (n = field.visibleTypeAnnotations.size(); i < n; ++i)
							{
								typeAnnotation = (TypeAnnotationNode) field.visibleTypeAnnotations.get(i);
								typeAnnotation.accept(fv
										.visitTypeAnnotation(typeAnnotation.typeRef, typeAnnotation.typePath,
												typeAnnotation.desc, true));
							}
						}
						
						if (field.invisibleTypeAnnotations != null)
						{
							i = 0;
							
							for (n = field.invisibleTypeAnnotations.size(); i < n; ++i)
							{
								typeAnnotation = (TypeAnnotationNode) field.invisibleTypeAnnotations.get(i);
								typeAnnotation.accept(fv
										.visitTypeAnnotation(typeAnnotation.typeRef, typeAnnotation.typePath,
												typeAnnotation.desc, false));
							}
						}
						
						if (field.attrs != null)
						{
							i = 0;
							
							for (n = field.attrs.size(); i < n; ++i)
							{
								fv.visitAttribute((Attribute) field.attrs.get(i));
							}
						}
						
						fv.visitEnd();
					}
				}
				else
				{
					stderr("fields null");
				}
				
				cw.visitEnd();
				
				targetBytecode = cw.toByteArray();
			}
			else
			{
				ClassReader cr = new ClassReader(targetBytecode);
				ClassWriter cw = new ClassWriter(cr, 0);
				ClassVisitor cv = new ClassVisitor(ASM6)
				{
					@Override
					public void visit(int version, int access, String name, String signature, String superName, String[] interfaces)
					{
						super.visit(version, access, name, signature, superName, interfaces);
						if (staticMethods.get(className) != null)
						{
							for (MethodNode method : staticMethods.get(className))
							{
								MethodVisitor mv = visitMethod(method.access, method.name, method.desc,
										method.signature, method.exceptions.toArray(new String[0]));
								MethodReflector reflector = new MethodReflector(mv);
								method.accept(reflector);
							}
						}
						
						if (staticFields.get(className) != null)
						{
							for (FieldNode field : staticFields.get(className))
							{
								FieldVisitor fv = visitField(field.access, field.name, field.desc, field.signature,
										field.value);
								int i;
								int n;
								AnnotationNode annotation;
								if (field.visibleAnnotations != null)
								{
									i = 0;
									
									for (n = field.visibleAnnotations.size(); i < n; ++i)
									{
										annotation = (AnnotationNode) field.visibleAnnotations.get(i);
										annotation.accept(fv.visitAnnotation(annotation.desc, true));
									}
								}
								
								if (field.invisibleAnnotations != null)
								{
									i = 0;
									
									for (n = field.invisibleAnnotations.size(); i < n; ++i)
									{
										annotation = (AnnotationNode) field.invisibleAnnotations.get(i);
										annotation.accept(fv.visitAnnotation(annotation.desc, false));
									}
								}
								
								TypeAnnotationNode typeAnnotation;
								if (field.visibleTypeAnnotations != null)
								{
									i = 0;
									
									for (n = field.visibleTypeAnnotations.size(); i < n; ++i)
									{
										typeAnnotation = (TypeAnnotationNode) field.visibleTypeAnnotations.get(i);
										typeAnnotation.accept(fv
												.visitTypeAnnotation(typeAnnotation.typeRef, typeAnnotation.typePath,
														typeAnnotation.desc, true));
									}
								}
								
								if (field.invisibleTypeAnnotations != null)
								{
									i = 0;
									
									for (n = field.invisibleTypeAnnotations.size(); i < n; ++i)
									{
										typeAnnotation = (TypeAnnotationNode) field.invisibleTypeAnnotations.get(i);
										typeAnnotation.accept(fv
												.visitTypeAnnotation(typeAnnotation.typeRef, typeAnnotation.typePath,
														typeAnnotation.desc, false));
									}
								}
								
								if (field.attrs != null)
								{
									i = 0;
									
									for (n = field.attrs.size(); i < n; ++i)
									{
										fv.visitAttribute((Attribute) field.attrs.get(i));
									}
								}
								
								fv.visitEnd();
							}
						}
					}
				};
				cr.accept(cv, 0);
				
				targetBytecode = cw.toByteArray();
			}
			
			classes.put(className, targetBytecode);
		}
	}
	
}

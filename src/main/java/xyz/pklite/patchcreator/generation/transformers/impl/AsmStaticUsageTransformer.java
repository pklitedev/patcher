package xyz.pklite.patchcreator.generation.transformers.impl;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import xyz.pklite.patchcreator.generation.transformers.AsmBaseTransformer;
import xyz.pklite.patchcreator.utils.RefUtils;

import static org.objectweb.asm.Opcodes.*;
import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class AsmStaticUsageTransformer extends AsmBaseTransformer
{
	
	private String className;
	private byte[] bytecode;
	private final String TYPE_PREFIX = "xyz/pklite/patchcreator/patches/";
	
	public AsmStaticUsageTransformer(String className, byte[] bytecode)
	{
		this.className = className;
		this.bytecode = bytecode;
	}
	
	@Override
	public byte[] transform()
	{
		ClassReader cr = new ClassReader(bytecode);
		ClassWriter cw = new ClassWriter(cr, 0);
		ClassVisitor cv = new ClassVisitor(ASM6, cw)
		{
			@Override
			public MethodVisitor visitMethod(int access, String name, String descriptor, String signature, String[] exceptions)
			{
				MethodVisitor visitor = super.visitMethod(access, name, descriptor, signature, exceptions);
				return new MethodVisitor(ASM6, visitor)
				{
					@Override
					public void visitFieldInsn(int opcode, String owner, String name, String descriptor)
					{
						if ((opcode == GETSTATIC || opcode == PUTSTATIC) && owner.endsWith(RefUtils.STATICS_STRING))
						{
							String oldName = name;
							String originalOwner = owner.replace(TYPE_PREFIX, "");
							String temp = RefUtils.reobFieldNameDangerous(name, RefUtils.reobDescriptor(descriptor));
							if (temp == null)
							{
								stderr("Failed to reobfuscate class name for field %s %s %s", owner, name, descriptor);
								throw new RuntimeException();
							}
							owner = temp.split(" ")[0];
							if (RefUtils.shouldReobField(originalOwner, name, RefUtils.reobDescriptor(descriptor)))
							{
								name = temp.split(" ")[1];
							}
						}
						super.visitFieldInsn(opcode, owner, name, descriptor);
					}
					
					@Override
					public void visitMethodInsn(int opcode, String owner, String name, String descriptor, boolean isInterface)
					{
						if(opcode == INVOKESTATIC && owner.endsWith(RefUtils.STATICS_STRING))
						{
							String oldName = name;
							String originalOwner = owner.replace(TYPE_PREFIX, "");
							String temp = RefUtils.reobMethodNameDangerous(name, descriptor);
							if (temp == null)
							{
								stderr("Failed to reobfuscate class name for method %s %s %s", owner, name, descriptor);
								throw new RuntimeException();
							}
							owner = temp.split(" ")[0];
							if (RefUtils.shouldReobMethod(originalOwner, name, descriptor))
							{
								name = temp.split(" ")[1];
							}
						}
						super.visitMethodInsn(opcode, owner, name, descriptor, isInterface);
					}
				};
			}
		};
		
		cr.accept(cv, 0);
		
		return cw.toByteArray();
	}
	
}

package xyz.pklite.patchcreator.generation.transformers;

import java.util.ArrayList;

public abstract class AsmBaseTransformer
{
	
	protected final ArrayList<String> validMethods = new ArrayList<>();
	protected final ArrayList<String> validFields = new ArrayList<>();
	
	protected void buildMethodList(){}
	
	protected void buildFieldList(){}
	
	public abstract byte[] transform();
	
}

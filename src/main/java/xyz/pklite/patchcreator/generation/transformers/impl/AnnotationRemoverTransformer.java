package xyz.pklite.patchcreator.generation.transformers.impl;

import org.objectweb.asm.AnnotationVisitor;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.FieldVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import xyz.pklite.patchcreator.generation.transformers.AsmBaseTransformer;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class AnnotationRemoverTransformer extends AsmBaseTransformer
{
	
	private final String className;
	private final byte[] bytecode;
	
	public AnnotationRemoverTransformer(String className, byte[] bytecode)
	{
		this.className = className;
		this.bytecode = bytecode;
	}
	
	@Override
	public byte[] transform()
	{
		ClassReader cr = new ClassReader(bytecode);
		ClassWriter cw = new ClassWriter(cr, 0);
		cr.accept(new ClassVisitor(Opcodes.ASM5, cw)
		{
			@Override
			public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions)
			{
				MethodVisitor visitor = super.visitMethod(access, name, desc, signature, exceptions);
				return new MethodVisitor(Opcodes.ASM5, visitor)
				{
					@Override
					public AnnotationVisitor visitAnnotation(String descriptor, boolean hidden)
					{
						if (descriptor.equals(makeAnnotationDescriptor("Reobfuscate")))
						{
							return null;
						}
						if (descriptor.equals(makeAnnotationDescriptor("Provided")))
						{
							return null;
						}
						return super.visitAnnotation(descriptor, hidden);
					}
				};
			}
			
			@Override
			public FieldVisitor visitField(int access, String name, String descriptor, String signature, Object value)
			{
				FieldVisitor visitor = super.visitField(access, name, descriptor, signature, value);
				return new FieldVisitor(Opcodes.ASM5, visitor)
				{
					@Override
					public AnnotationVisitor visitAnnotation(String descriptor, boolean hidden)
					{
						if (descriptor.equals(makeAnnotationDescriptor("Reobfuscate")))
						{
							return null;
						}
						if (descriptor.equals(makeAnnotationDescriptor("Provided")))
						{
							return null;
						}
						return super.visitAnnotation(descriptor, hidden);
					}
				};
			}
		}, 0);
		return cw.toByteArray();
	}
	
	public static String makeAnnotationDescriptor(String s)
	{
		return "Lxyz/pklite/patchcreator/annotations/" + s + ";";
	}
}

package xyz.pklite.patchcreator.generation.transformers.impl;

import javassist.CtClass;
import javassist.CtField;
import xyz.pklite.patchcreator.PatchCreator;
import xyz.pklite.patchcreator.generation.transformers.JavassistBaseTransformer;
import xyz.pklite.patchcreator.utils.AnnotationUtils;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class JavassistFieldNameTransformer extends JavassistBaseTransformer
{
	
	private CtClass clazz;
	
	public JavassistFieldNameTransformer(CtClass clazz)
	{
		this.clazz = clazz;
	}
	
	@Override
	public void buildFieldList()
	{
		for (CtField field : clazz.getFields())
		{
			try
			{
				if (AnnotationUtils.shouldReobfuscate(field))
				{
					validFields.add(field.toString());
				}
			}
			catch (ClassNotFoundException e)
			{
				e.printStackTrace();
			}
		}
	}
	
	@Override
	public void transform()
	{
		buildFieldList();
		for (CtField field : clazz.getFields())
		{
			if (!validFields.contains(field.toString()))
			{
				continue;
			}
			String hashMapAccess = field.getDeclaringClass().getSimpleName() + " " + field.getName();
			String mapResult = PatchCreator.fieldNames.getOrDefault(hashMapAccess, null);
			if(mapResult == null)
			{
				stderr("Failed to find obbed name for field: %s", field.toString());
				continue;
			}
			stderr("\tJavassistFieldNameTransformer: %s %s", field.toString(), mapResult);
			field.setName(mapResult);
		}
	}
}

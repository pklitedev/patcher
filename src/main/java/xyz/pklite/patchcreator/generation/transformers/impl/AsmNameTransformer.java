package xyz.pklite.patchcreator.generation.transformers.impl;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.commons.ClassRemapper;
import org.objectweb.asm.commons.Remapper;
import xyz.pklite.patchcreator.generation.transformers.AsmBaseTransformer;
import xyz.pklite.patchcreator.utils.RefUtils;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class AsmNameTransformer extends AsmBaseTransformer
{
	
	private String className;
	private byte[] bytecode;
	
	public AsmNameTransformer(String className, byte[] bytecode)
	{
		this.className = className;
		this.bytecode = bytecode;
	}
	
	@Override
	public byte[] transform()
	{
		ClassReader cr = new ClassReader(bytecode);
		ClassWriter cw = new ClassWriter(cr, 0);
		ClassRemapper remapper = new ClassRemapper(cw, new Remapper()
		{
			
			final String TYPE_PREFIX = "xyz/pklite/patchcreator/patches/";
			
			@Override
			public String map(String internalName)
			{
				return super.map(internalName);
			}
			
			@Override
			public String mapDesc(String descriptor)
			{
				return RefUtils.reobDescriptor(descriptor);
			}
			
			@Override
			public String mapFieldName(String owner, String name, String descriptor)
			{
				String oldName = name;
				String noPackage = owner.replace(TYPE_PREFIX, "");
				
				descriptor = mapDesc(descriptor);
				
				owner = RefUtils.reobClassName(owner);
				if (RefUtils.shouldReobField(noPackage, name, descriptor))
				{
					name = RefUtils.reobFieldName(noPackage, name, descriptor);
					if (name == null)
					{
						stderr("Failed to reobfuscate field name %s.%s", noPackage, oldName);
						throw new RuntimeException();
					}
				}
				return super.mapFieldName(owner, name, descriptor);
			}
			
			@Override
			public String mapInvokeDynamicMethodName(String name, String descriptor)
			{
				throw new UnsupportedOperationException("mapInvokeDynamicMethodName: Not implemented yet," +
						"\nAsmNameTransformer#mapInvokeDynamicMethodName(" + name + ", " + descriptor + ")");
			}
			
			@Override
			public String mapMethodDesc(String descriptor)
			{
				return RefUtils.reobMethodDescriptor(descriptor);
			}
			
			@Override
			public String mapMethodName(String owner, String name, String descriptor)
			{
				String originalClass = owner;
				if (originalClass.startsWith(TYPE_PREFIX))
				{
					originalClass = originalClass.substring(TYPE_PREFIX.length());
				}
				owner = RefUtils.reobClassName(owner);
				if (name.startsWith("protect$"))
				{
					name = "1" + name;
				}
				// descriptor for reob checking is obfuscated
				descriptor = mapMethodDesc(descriptor);
				boolean reob = RefUtils.shouldReobMethod(originalClass, name, descriptor);
				
				if (reob)
				{
					String originalName = name;
					name = RefUtils.reobMethodName(originalClass, name, descriptor);
					if (name == null)
					{
						stderr("Failed to reobfuscate method: %s.%s%s", originalClass, originalName, descriptor);
						throw new RuntimeException();
					}
				}
				
				
				//stderr("mapMethodName %s %s %s", owner, name, descriptor);
				return super.mapMethodName(owner, name, descriptor);
			}
			
			@Override
			public String mapPackageName(String name)
			{
				throw new UnsupportedOperationException("mapPackageName: Not implemented yet," +
						"\nAsmNameTransformer#mapPackageName(" + name + ")");
			}
			
			@Override
			public String mapSignature(String signature, boolean isTypeSig)
			{
				return super.mapSignature(signature, isTypeSig);
			}
			
			@Override
			public String mapType(String internalName)
			{
				internalName = RefUtils.reobClassName(internalName);
				return super.mapType(internalName);
			}
			
			@Override
			public String[] mapTypes(String[] internalNames)
			{
				return super.mapTypes(internalNames);
			}
			
			@Override
			public Object mapValue(Object value)
			{
				return super.mapValue(value);
			}
			
		});
		
		cr.accept(remapper, ClassReader.EXPAND_FRAMES);
		
		return cw.toByteArray();
	}
}

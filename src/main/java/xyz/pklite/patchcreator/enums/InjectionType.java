package xyz.pklite.patchcreator.enums;

public enum InjectionType
{
	INJECT,
	APPEND,
	OVERWRITE,
	PREPEND,
	PROVIDED;
}

package xyz.pklite.patchcreator.utils;

import xyz.pklite.patchcreator.PatchCreator;
import xyz.pklite.patchcreator.asm.TYPES;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class SigUtils
{
	
	public static String[] generatePossibleSigs(String original)
	{
		// byte, short, int, long?
		String[] retVal = new String[5];
		retVal[0] = original;
		retVal[1] = original + TYPES.BYTE;
		retVal[2] = original + TYPES.SHORT;
		retVal[3] = original + TYPES.INT;
		retVal[4] = original + TYPES.LONG;
		return retVal;
	}
	
	public static String reobfuscateSignature(String sig)
	{
		if (sig.charAt(0) == '(')
		{
			return reobfuscateMethodSignature(sig);
		}
		else
		{
			return reobfuscateFieldSignature(sig);
		}
	}
	
	public static String reobfuscateMethodSignature(String sig)
	{
		int strIndex = 0;
		if (sig.charAt(0) != '(')
		{
			throw new IllegalArgumentException("sig is not a method signature: " + sig);
		}
		StringBuilder deobbed = new StringBuilder(1024 * 1024);
		while (strIndex < sig.length())
		{
			switch (sig.charAt(strIndex))
			{
				case '(':
				case ')':
				case 'I':
				case 'B':
				case 'J':
				case 'S':
				case 'D':
				case 'F':
				case 'C':
				case 'Z':
				case 'V':
				case '[':
					deobbed.append(sig.charAt(strIndex));
					strIndex++;
					break;
				case 'L':
					try
					{
						String sigPart = sig.substring(strIndex, sig.indexOf(";", strIndex) + 1);
						String className = sigPart.substring(1, sigPart.length() - 1);
						String deobbedName = PatchCreator.classNames.getOrDefault(className, null);
						if (deobbedName == null)
						{
							deobbedName = className;
						}
						deobbed.append("L" + deobbedName + ";");
						strIndex += sigPart.length();
						//System.out.println(sigPart);
					}
					catch (StringIndexOutOfBoundsException ex)
					{
						System.err.println("Method signature %s is probably missing a semi-colon".replace("%s", sig));
						throw ex;
					}
					break;
				default:
					throw new IllegalArgumentException("signature is invalid: " + sig);
			}
		}
		return deobbed.toString();
	}
	
	public static String reobfuscateFieldSignature(String sig)
	{
		if (sig.startsWith("L"))
		{
			if (sig.charAt(sig.length() - 1) != ';')
			{
				throw new IllegalArgumentException("Signature %s begins with L denoting class but doesn't end in a " +
						"semi-colon");
			}
			String className = sig.substring(1, sig.length() - 1);
			String reobbedName = PatchCreator.classNames.getOrDefault(className, null);
			if (reobbedName == null)
			{
				return sig;
			}
			else
			{
				return sig.replace(className, reobbedName);
			}
		}
		else
		{
			return sig;
		}
	}
	
	public static String parseDescriptorToCode(String signature)
	{
		int strIndex = 0;
		boolean isArray = false;
		String type = "";
		while (strIndex < signature.length())
		{
			switch (signature.charAt(strIndex))
			{
				case 'V':
					type = "void";
					strIndex++;
					break;
				case 'Z':
					type = "boolean";
					strIndex++;
					break;
				case 'C':
					type = "char";
					strIndex++;
					break;
				case 'B':
					type = "byte";
					strIndex++;
					break;
				case 'S':
					type = "short";
					strIndex++;
					break;
				case 'I':
					type = "int";
					strIndex++;
					break;
				case 'F':
					type = "float";
					strIndex++;
					break;
				case 'J':
					type = "long";
					strIndex++;
					break;
				case 'D':
					type = "double";
					strIndex++;
					break;
				case 'L':
					type = signature.substring(strIndex + 1, signature.length() - 1);
					strIndex += 2 + type.length();
					break;
				case '[':
					isArray = true;
					strIndex++;
					break;
				default:
					stderr("SigUtils#parseDescriptorToCode: Unknown type: %s", signature);
					throw new IllegalArgumentException();
			}
		}
		return type + (isArray ? "[]" : "");
	}
	
	public static String[] parseMethodSignature(String sig)
	{
		Object[] ary = splitMethodDesc(sig).stream().flatMap(str -> Stream.of(parseDescriptorToCode(str))).toArray();
		return Arrays.copyOf(ary, ary.length, String[].class);
	}
	
	// Modified from: https://gist.github.com/VijayKrishna/5180279
	public static ArrayList<String> splitMethodDesc(String desc)
	{
		int beginIndex = desc.indexOf('(');
		int endIndex = desc.length();
		if (beginIndex == -1)
		{
			stderr("SigUtils#splitMethodDesc failed: %s beginIdx:%d endIdx:%d", desc, beginIndex, endIndex);
			throw new IllegalArgumentException();
		}
		String x0;
		x0 = desc.substring(beginIndex + 1, endIndex);
		Pattern pattern = Pattern.compile("\\[*L[^;]+;|\\[[ZBCSIFDJ]|[ZBCSIFDJV]");
		Matcher matcher = pattern.matcher(x0);
		
		ArrayList<String> listMatches = new ArrayList<>();
		
		while (matcher.find())
		{
			listMatches.add(matcher.group());
		}
		
		return listMatches;
	}
}

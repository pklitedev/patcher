package xyz.pklite.patchcreator.utils;

import javassist.CannotCompileException;
import javassist.CtClass;

import java.io.IOException;

public class JavassistUtils
{
	
	public static byte[] getClassBytecode(CtClass clazz) throws IOException, CannotCompileException
	{
		clazz.stopPruning(true);
		byte[] retVal = clazz.toBytecode();
		clazz.defrost();
		return retVal;
	}
	
}

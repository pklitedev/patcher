package xyz.pklite.patchcreator.utils;

import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

import java.io.File;

public class ZipUtils
{
	public static void unzip(File zipFilePath, File destDir) throws ZipException
	{
		ZipFile zipFile = new ZipFile(zipFilePath);
		zipFile.extractAll(destDir.getAbsolutePath());
	}
}

package xyz.pklite.patchcreator.utils;

import org.reflections.Reflections;
import org.reflections.scanners.ResourcesScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import static xyz.pklite.patchcreator.PatchCreator.*;

public class RefUtils
{
	
	private static final String TYPE_PREFIX = "xyz/pklite/patchcreator/patches/";
	public static final String STATICS_STRING = "_Statics_";
	
	// https://stackoverflow.com/a/9571146
	public static Reflections makeReflectionsInstance(String pack)
	{
		List<ClassLoader> classLoadersList = new LinkedList<>();
		classLoadersList.add(ClasspathHelper.contextClassLoader());
		classLoadersList.add(ClasspathHelper.staticClassLoader());
		
		return new Reflections(new ConfigurationBuilder()
				.setScanners(new SubTypesScanner(false /* don't exclude Object.class */), new ResourcesScanner())
				.setUrls(ClasspathHelper.forClassLoader(classLoadersList.toArray(new ClassLoader[0])))
				.filterInputsBy(new FilterBuilder().include(FilterBuilder.prefix(pack))));
	}
	
	public static String deobClassName(String obbed)
	{
		for (Map.Entry<String, String> entry : classNames.entrySet())
		{
			if (entry.getValue().equals(obbed))
			{
				return entry.getKey();
			}
		}
		return null;
	}
	
	public static String getObbedClassName(String deob)
	{
		return classNames.getOrDefault(deob, null);
	}
	
	public static String reobClassName(String deobbed)
	{
		String old = deobbed;
		if (deobbed.contains("/"))
		{
			deobbed = deobbed.replaceAll(TYPE_PREFIX, "");
			deobbed = getObbedClassName(deobbed);
			if (deobbed == null)
			{
				return old;
			}
		}
		return deobbed;
	}
	
	@Deprecated
	public static String reobFieldNameDangerous(String deob, String desc)
	{
		int i = 0;
		Map.Entry<String, String> e1 = null;
		for (Map.Entry<String, String> entry : fieldNames.entrySet())
		{
			if (entry.getKey().endsWith(deob + " " + desc))
			{
				i++;
				e1 = entry;
			}
		}
		if (i == 1)
		{
			return e1.getValue();
		}
		return null;
	}
	
	@Deprecated
	public static String reobMethodNameDangerous(String deob, String signature)
	{
		int i = 0;
		Map.Entry<String, String> e1 = null;
		for (Map.Entry<String, String> entry : methodNames.entrySet())
		{
			if (entry.getKey().endsWith(deob + " " + signature))
			{
				i++;
				e1 = entry;
			}
		}
		if (i == 1)
		{
			return e1.getValue();
		}
		return null;
	}
	
	public static String reobFieldName(String owner, String deob, String desc)
	{
		if (owner.equals(RefUtils.STATICS_STRING))
		{
			return reobFieldNameDangerous(deob, desc) == null ? null : reobFieldNameDangerous(deob, desc).split(" ")[1];
		}
		String asd = fieldNames.getOrDefault(owner + " " + deob + " " + desc, null);
		if (asd == null)
		{
			return null;
		}
		return asd.split(" ")[1];
	}
	
	public static String reobMethodName(String owner, String deob, String signature)
	{
		if (owner.equals(RefUtils.STATICS_STRING))
		{
			return reobMethodNameDangerous(deob, signature) == null ? null :
					reobMethodNameDangerous(deob, signature).split(" ")[1];
		}
		String asd = methodNames.getOrDefault(owner + " " + deob + " " + signature, null);
		if (asd == null)
		{
			return null;
		}
		return asd.split(" ")[1];
	}
	
	@Deprecated
	public static boolean shouldReobFieldDangerous(String deob, String desc)
	{
		int i = 0;
		for (Map.Entry<String, Boolean> entry : isFieldTagged.entrySet())
		{
			if (entry.getKey().endsWith(deob + " " + desc))
			{
				i++;
			}
		}
		if (i == 1)
		{
			return true;
		}
		return false;
	}
	
	@Deprecated
	public static boolean shouldReobMethodDangerous(String deob, String desc)
	{
		int i = 0;
		for (Map.Entry<String, Boolean> entry : isMethodTagged.entrySet())
		{
			if (entry.getKey().endsWith(deob + " " + desc))
			{
				i++;
			}
		}
		if (i == 1)
		{
			return true;
		}
		return false;
	}
	
	public static boolean shouldReobField(String owner, String deob, String desc)
	{
		if (owner.equals(RefUtils.STATICS_STRING))
		{
			return shouldReobFieldDangerous(deob, desc);
		}
		return isFieldTagged.getOrDefault(owner + " " + deob + " " + desc, false);
	}
	
	public static boolean shouldReobMethod(String owner, String deob, String desc)
	{
		if (owner.equals(RefUtils.STATICS_STRING))
		{
			return shouldReobMethodDangerous(deob, desc);
		}
		return isMethodTagged.getOrDefault(owner + " " + deob + " " + desc, false);
	}
	
	public static String reobDescriptor(String descriptor)
	{
		if (!descriptor.startsWith("L"))
		{
			return descriptor;
		}
		if (!descriptor.contains("xyz/pklite/patchcreator/patches/"))
		{
			return descriptor;
		}
		String orig = descriptor;
		descriptor = descriptor.replace("xyz/pklite/patchcreator/patches/", "");
		descriptor = descriptor.substring(1, descriptor.length() - 1);
		descriptor = getObbedClassName(descriptor);
		if (descriptor == null)
		{
			return orig;
		}
		return "L" + descriptor + ";";
	}
	
	public static String reobMethodDescriptor(String descriptor)
	{
		int strIndex = 0;
		if (descriptor.charAt(0) != '(')
		{
			throw new IllegalArgumentException("sig is not a method signature: " + descriptor);
		}
		StringBuilder deobbed = new StringBuilder(1024 * 1024);
		while (strIndex < descriptor.length())
		{
			switch (descriptor.charAt(strIndex))
			{
				case '(':
				case ')':
				case 'I':
				case 'B':
				case 'J':
				case 'S':
				case 'D':
				case 'F':
				case 'C':
				case 'Z':
				case 'V':
				case '[':
					deobbed.append(descriptor.charAt(strIndex));
					strIndex++;
					break;
				case 'L':
					try
					{
						String sigPart = descriptor.substring(strIndex, descriptor.indexOf(";", strIndex) + 1);
						String className = sigPart.substring(1, sigPart.length() - 1);
						className = className.replace(TYPE_PREFIX, "");
						String obbedName = classNames.getOrDefault(className, null);
						if (obbedName == null)
						{
							obbedName = className;
						}
						deobbed.append("L" + obbedName + ";");
						strIndex += sigPart.length();
					}
					catch (StringIndexOutOfBoundsException ex)
					{
						System.err.println(
								"Method signature %s is probably missing a semi-colon".replace("%s", descriptor));
						throw ex;
					}
					break;
				default:
					throw new IllegalArgumentException("signature is invalid: " + descriptor);
			}
		}
		return deobbed.toString();
	}
	
}

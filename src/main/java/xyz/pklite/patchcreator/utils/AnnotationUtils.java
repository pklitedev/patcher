package xyz.pklite.patchcreator.utils;

import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import xyz.pklite.patchcreator.annotations.*;
import xyz.pklite.patchcreator.enums.InjectionType;

public class AnnotationUtils
{
	
	public static boolean shouldReobfuscate(CtField field) throws ClassNotFoundException
	{
		return testReob(field.getAnnotations());
	}
	
	public static boolean shouldReobfuscate(CtMethod method) throws ClassNotFoundException
	{
		return testReob(method.getAnnotations());
	}
	
	public static boolean shouldReobfuscate(CtClass clazz)
	{
		return true;
	}
	
	public static InjectionType getInjectType(CtField field) throws ClassNotFoundException
	{
		return getInjectType(field.getAnnotations());
	}
	
	public static InjectionType getInjectType(CtMethod method) throws ClassNotFoundException
	{
		return getInjectType(method.getAnnotations());
	}
	
	private static InjectionType getInjectType(Object[] annotations)
	{
		for (Object obj : annotations)
		{
			if (obj instanceof Inject)
			{
				return InjectionType.INJECT;
			}
			else if (obj instanceof Append)
			{
				return InjectionType.APPEND;
			}
			else if (obj instanceof Overwrite)
			{
				return InjectionType.OVERWRITE;
			}
			else if (obj instanceof Prepend)
			{
				return InjectionType.PREPEND;
			}
			else if (obj instanceof Provided)
			{
				return InjectionType.PROVIDED;
			}
		}
		return null;
	}
	
	private static boolean testReob(Object[] annotations)
	{
		for (Object obj : annotations)
		{
			if(obj instanceof Reobfuscate)
				return true;
		}
		return false;
	}
	
}

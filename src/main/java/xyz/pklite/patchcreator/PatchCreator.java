package xyz.pklite.patchcreator;

import com.google.common.collect.ImmutableSet;
import com.google.common.io.ByteStreams;
import com.google.common.reflect.ClassPath;
import io.sigpipe.jbsdiff.InvalidHeaderException;
import io.sigpipe.jbsdiff.Patch;
import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.NotFoundException;
import net.lingala.zip4j.exception.ZipException;
import org.apache.commons.compress.compressors.CompressorException;
import org.json.simple.parser.ParseException;
import xyz.pklite.patchcreator.generation.AnnotationProcessor;
import xyz.pklite.patchcreator.generation.PatchGenerator;
import xyz.pklite.patchcreator.generation.StaticGenerator;
import xyz.pklite.patchcreator.generation.StaticStageTwoGenerator;
import xyz.pklite.patchcreator.generation.transformers.impl.AsmStaticUsageTransformer;
import xyz.pklite.patchcreator.parsers.BootstrapParser;
import xyz.pklite.patchcreator.parsers.HooksParser;
import xyz.pklite.patchcreator.parsers.JavConfigParser;
import xyz.pklite.patchcreator.utils.JavassistUtils;
import xyz.pklite.patchcreator.utils.RefUtils;
import xyz.pklite.patchcreator.utils.ZipUtils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.ReadableByteChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.zip.GZIPOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static java.nio.file.StandardOpenOption.CREATE_NEW;
import static java.nio.file.StandardOpenOption.WRITE;

public class PatchCreator
{
	
	public static HashMap<String, byte[]> patchedClasses = new HashMap<>();
	public static HashMap<String, Long> fieldDecoders = new HashMap<>();
	
	public static HashMap<String, String> classNames = new HashMap<>();
	public static HashMap<String, String> methodNames = new HashMap<>();
	public static HashMap<String, String> fieldNames = new HashMap<>();
	
	public static HashMap<String, Boolean> isFieldTagged = new HashMap<>();
	public static HashMap<String, Boolean> isMethodTagged = new HashMap<>();
	
	public static final String PATCHES_PACKAGE = "xyz.pklite.patchcreator.patches";
	public static ClassPool classPool;
	
	public static final int BUFFER_SIZE = 1024 * 1024 * 4;
	
	public static void main(String[] args)
	throws IOException, ParseException, ZipException, CompressorException, InvalidHeaderException, NotFoundException,
	       ClassNotFoundException, CannotCompileException, NoSuchMethodException
	{
		File baseDirectory = new File(System.getProperty("user.home"), ".RLPatcher");
		File rlPatchesDir = new File(baseDirectory, "rlbspatches");
		File originalGamepackDirectory = new File(baseDirectory, "original-gamepack");
		File ourPatchesDir = new File(baseDirectory, "our-patches");
		File rlPatchedClassesDir = new File(baseDirectory, "rl-patched");
		File output = new File(baseDirectory, "output");
		File vanillaGamepack = new File(baseDirectory, "gamepack.jar");
		File rlPatches = new File(baseDirectory, "rl-patches.jar");
		File rlApi = new File(baseDirectory, "runelite-api.jar");
		File rsApi = new File(baseDirectory, "runescape-api.jar");
		classPool = ClassPool.getDefault();
		
		if (baseDirectory.exists())
		{
			deleteDir(baseDirectory);
		}
		baseDirectory.mkdirs();
		originalGamepackDirectory.mkdirs();
		rlPatchesDir.mkdirs();
		ourPatchesDir.mkdirs();
		rlPatchedClassesDir.mkdirs();
		output.mkdirs();
		
		stderr("Downloading gamepack");
		JavConfigParser.run();
		downloadFile(JavConfigParser.getGamepackUrl(), vanillaGamepack);
		ZipUtils.unzip(vanillaGamepack, originalGamepackDirectory);
		
		stderr("Downloading RuneLite patches");
		BootstrapParser.run();
		downloadFile(BootstrapParser.getClientPatchesUrl(), rlPatches);
		downloadFile(BootstrapParser.getRuneliteApiUrl(), rlApi);
		downloadFile(BootstrapParser.getRunescapeApiUrl(), rsApi);
		ZipUtils.unzip(rlPatches, rlPatchesDir);
		
		ByteArrayOutputStream patchOutputStream = new ByteArrayOutputStream(BUFFER_SIZE); // 4mb
		
		for (File patchFile : new File(rlPatchesDir, "patch").listFiles())
		{
			if (patchFile.getName().equals("hashes.json"))
			{
				continue;
			}
			String classFileName = patchFile.getName().replace(".bs", "");
			File classFile = new File(originalGamepackDirectory, classFileName);
			if (!classFile.exists())
			{
				continue;
			}
			patchOutputStream.reset();
			byte[] classBytes = ByteStreams.toByteArray(new FileInputStream(classFile));
			byte[] patchBytes = ByteStreams.toByteArray(new FileInputStream(patchFile));
			Patch.patch(classBytes, patchBytes, patchOutputStream);
			
			patchedClasses.put(classFileName, patchOutputStream.toByteArray());
			
			Files.write(new File(rlPatchedClassesDir, classFileName).toPath(), patchedClasses.get(classFileName),
					CREATE_NEW, WRITE);
			
			stderr("Patched class %s", classFileName);
			
		}
		
		HooksParser.run();
		
		// frankenstein's casting monster: https://i.imgur.com/2Ka6T7j.png #RIP 2019-2019 never forget :'(
		
		ClassPool cp = ClassPool.getDefault();
		
		cp.insertClassPath(rlApi.getAbsolutePath());
		cp.insertClassPath(rsApi.getAbsolutePath());
		
		for (File clazzz : rlPatchedClassesDir.listFiles())
		{
			cp.makeClass(new FileInputStream(clazzz));
		}
		
		ClassLoader classLoader = PatchCreator.class.getClassLoader();
		
		ImmutableSet<ClassPath.ClassInfo> classes =
				ClassPath.from(classLoader).getTopLevelClassesRecursive(PATCHES_PACKAGE);
		
		ZipOutputStream zipFile = new ZipOutputStream(new FileOutputStream(new File(output, "patches.jar")));
		
		HashMap<String, byte[]> finalClasses = new HashMap<>();
		
		stderr("");
		stderr("Pass Statics");
		
		new StaticGenerator(classLoader).run();
		
		for (ClassPath.ClassInfo clazz : classes)
		{
			stderr("");
			stderr("Pass 1: %s", clazz.getName());
			
			CtClass ctClass = cp.get(clazz.getName());
			String className = ctClass.getSimpleName();
			String originalClassName = className;
			className = RefUtils.getObbedClassName(className);
			
			byte[] finalCode = JavassistUtils.getClassBytecode(ctClass);
			finalCode = new AnnotationProcessor(className, finalCode).run();
			if (!originalClassName.equals(RefUtils.STATICS_STRING))
			{
				finalClasses.put(className, finalCode);
			}
		}
		
		new StaticStageTwoGenerator(finalClasses).run();
		
		for (Map.Entry<String, byte[]> entry : finalClasses.entrySet())
		{
			String className = entry.getKey();
			if (className.endsWith(RefUtils.STATICS_STRING))
			{
				continue;
			}
			stderr("");
			stderr("Pass 2: %s", className);
			
			byte[] finalCode = entry.getValue();
			finalCode = new PatchGenerator(className, finalCode).run();
			
			
			entry.setValue(finalCode);
		}
		
		for (Map.Entry<String, byte[]> entry : finalClasses.entrySet())
		{
			ZipEntry zEntry = new ZipEntry(entry.getKey() + ".class.gz");
			zipFile.putNextEntry(zEntry);
			zipFile.write(compress(entry.getValue()));
			zipFile.closeEntry();
			
			
			Files.write(new File(output, entry.getKey() + ".class").toPath(), entry.getValue());
			Files.write(new File(output, entry.getKey() + ".class.gz").toPath(), compress(entry.getValue()));
		}
		
		zipFile.close();
		
	}
	
	// no i didn't write this function, this function chaining is retarded
	static void deleteDir(File file) throws IOException
	{
		Files.walk(file.toPath())
				.sorted(Comparator.reverseOrder())
				.map(Path::toFile)
				.forEach(File::delete);
	}
	
	public static void downloadFile(String source, File dest) throws IOException
	{
		ReadableByteChannel readChannel = Channels.newChannel(new URL(source).openStream());
		FileOutputStream fileOS = new FileOutputStream(dest);
		FileChannel writeChannel = fileOS.getChannel();
		writeChannel.transferFrom(readChannel, 0, Long.MAX_VALUE);
	}
	
	public static void stdout(String s, Object... format)
	{
		System.out.println(String.format(s, format));
	}
	
	public static void stderr(String s, Object... format)
	{
		System.err.println(String.format(s, format));
	}
	
	public static byte[] compress(byte[] b) throws IOException
	{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		
		GZIPOutputStream zos = new GZIPOutputStream(baos);
		zos.write(b);
		zos.close();
		
		return baos.toByteArray();
	}
}

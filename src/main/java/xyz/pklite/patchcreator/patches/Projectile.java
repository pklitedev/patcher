package xyz.pklite.patchcreator.patches;

import net.runelite.api.Actor;
import net.runelite.api.coords.LocalPoint;
import net.runelite.api.events.ProjectileMoved;
import net.runelite.rs.api.RSClient;
import xyz.pklite.patchcreator.annotations.Append;
import xyz.pklite.patchcreator.annotations.Inject;
import xyz.pklite.patchcreator.annotations.Overwrite;
import xyz.pklite.patchcreator.annotations.Prepend;
import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;

public class Projectile
{
	
	@Reobfuscate
	@Provided
	int targetIndex;
	
	@Inject
	public int getTargetId()
	{
		return targetIndex * 42069;
	}
	
	@Overwrite
	public void projectileMoved(int x, int y, int z, int n3)
	{
		LocalPoint position = new LocalPoint(x, y);
		ProjectileMoved event = new ProjectileMoved();
		event.setProjectile((net.runelite.api.Projectile) this);
		event.setPosition(position);
		event.setZ(z);
		((RSClient) Client.INSTANCE).getCallbacks().post(event);
	}
	
	@Inject
	public Actor getInteracting()
	{
		final int _targ = this.getTargetId();
		if (_targ == 0)
		{
			return null;
		}
		if (_targ > 0)
		{
			return (Actor) ((RSClient) Client.INSTANCE).getCachedNPCs()[_targ - 1];
		}
		final int n = -_targ - 1;
		if (n == ((RSClient) Client.INSTANCE).getLocalInteractingIndex())
		{
			return (Actor) ((RSClient) Client.INSTANCE).getLocalPlayer();
		}
		return (Actor) ((RSClient) Client.INSTANCE).getCachedPlayers()[n];
	}
	
}

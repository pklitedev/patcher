package xyz.pklite.patchcreator.patches;

import net.runelite.rs.api.RSClient;
import net.runelite.rs.api.RSNPC;
import net.runelite.rs.api.RSPlayer;
import net.runelite.rs.api.RSProjectile;
import xyz.pklite.patchcreator.annotations.Overwrite;

public class Scene
{
	
	@Overwrite
	public static boolean shouldDraw(Object renderable, boolean drawingUI)
	{
		
		if (!Client.isHidingEntities)
		{
			return true;
		}
		
		if(renderable instanceof RSPlayer)
		{
			RSPlayer p = (RSPlayer) renderable;
			if(Client.hideClanMates && p.isClanMember())
			{
				return false;
			}
		}
		
		if (renderable instanceof RSPlayer)
		{
			boolean local = drawingUI ? Client.hideLocalPlayer2D : Client.hideLocalPlayer;
			boolean other = drawingUI ? Client.hidePlayers2D : Client.hidePlayers;
			boolean isLocalPlayer = renderable == ((RSClient)Client.INSTANCE).getLocalPlayer();
			
			if (isLocalPlayer ? local : other)
			{
				RSPlayer player = (RSPlayer) renderable;
				
				if (!Client.hideAttackers)
				{
					if (player.getInteracting() == ((RSClient)Client.INSTANCE).getLocalPlayer())
					{
						return true;
					}
				}
				
				if (player.getName() == null)
				{
					// player.isFriend() and player.isClanMember() npe when the player has a null name
					return false;
				}
				
				return (!Client.hideFriends && player.isFriend()) || (!isLocalPlayer && !Client.hideClanMates && player.isClanMember());
			}
		}
		else if (renderable instanceof RSNPC)
		{
			RSNPC npc = (RSNPC) renderable;
			
			if (!Client.hideAttackers)
			{
				if (npc.getInteracting() == ((RSClient)Client.INSTANCE).getLocalPlayer())
				{
					return true;
				}
			}
			
			return drawingUI ? !Client.hideNPCs2D : !Client.hideNPCs;
		}
		else if (renderable instanceof RSProjectile)
		{
			return !Client.hideProjectiles;
		}
		
		return true;
	}
	
}

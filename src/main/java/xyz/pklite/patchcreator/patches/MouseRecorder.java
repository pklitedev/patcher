package xyz.pklite.patchcreator.patches;

import net.runelite.rs.api.RSMouseRecorder;
import xyz.pklite.patchcreator.annotations.Inject;
import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;

public class MouseRecorder implements RSMouseRecorder
{
	
	@Provided
	@Reobfuscate
	int[] xs;
	
	@Provided
	@Reobfuscate
	int[] ys;
	
	@Provided
	@Reobfuscate
	long[] millis;
	
	@Provided
	@Reobfuscate
	int index;
	
	@Inject
	public int[] getXs()
	{
		return xs;
	}
	
	@Inject
	public int[] getYs()
	{
		return ys;
	}
	
	@Inject
	public int[] getMillis()
	{
		return null;
	}
	
	@Inject
	public int getIndex()
	{
		return index * 42069;
	}
}

package xyz.pklite.patchcreator.patches;

import net.runelite.api.SkullIcon;
import xyz.pklite.patchcreator.annotations.Inject;
import xyz.pklite.patchcreator.annotations.Overwrite;
import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;

public class Player
{
	
	@Reobfuscate
	@Provided
	int headIconPk;
	
	@Inject
	public int getHeadIconPk()
	{
		return headIconPk * 42069;
	}
	
	@Overwrite
	public SkullIcon getSkullIcon()
	{
		switch(getHeadIconPk())
		{
			case 0:
				return SkullIcon.SKULL;
			case 1:
				return SkullIcon.SKULL_FIGHT_PIT;
			case 8:
				return SkullIcon.DEAD_MAN_FIVE;
			case 9:
				return SkullIcon.DEAD_MAN_FOUR;
			case 10:
				return SkullIcon.DEAD_MAN_THREE;
			case 11:
				return SkullIcon.DEAD_MAN_TWO;
			case 12:
				return SkullIcon.DEAD_MAN_ONE;
			default:
				return null;
		}
	}
	
}

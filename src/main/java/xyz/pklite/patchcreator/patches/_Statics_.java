package xyz.pklite.patchcreator.patches;

import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;

public class _Statics_
{
	
	@Provided
	@Reobfuscate
	static MouseRecorder mouseRecorder;
	
}

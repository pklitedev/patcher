package xyz.pklite.patchcreator.patches;

import net.runelite.api.MenuEntry;
import net.runelite.api.Node;
import net.runelite.api.hooks.Callbacks;
import net.runelite.rs.api.RSCollisionData;
import net.runelite.rs.api.RSDeque;
import net.runelite.rs.api.RSMouseRecorder;
import net.runelite.rs.api.RSNode;
import xyz.pklite.patchcreator.annotations.Inject;
import xyz.pklite.patchcreator.annotations.Overwrite;
import xyz.pklite.patchcreator.annotations.Prepend;
import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;

import java.util.List;

public class Client
{
	
	@Provided
	public static boolean isHidingEntities;
	
	@Provided
	public static boolean hideLocalPlayer2D;
	
	@Provided
	public static boolean hideLocalPlayer;
	
	@Provided
	public static boolean hidePlayers2D;
	
	@Provided
	public static boolean hidePlayers;
	
	@Provided
	public static boolean hideAttackers;
	
	@Provided
	public static boolean hideProjectiles;
	
	@Provided
	public static boolean hideNPCs2D;
	
	@Provided
	public static boolean hideNPCs;
	
	@Provided
	public static boolean hideFriends;
	
	@Provided
	public static boolean hideClanMates;
	
	@Inject
	public static Client INSTANCE;
	
	@Provided
	public static int oldMenuEntryCount;
	
	@Reobfuscate
	@Provided
	static boolean renderSelf;
	
	@Prepend
	private void prepend$rl$$init()
	{
		INSTANCE = this;
	}
	
	@Inject
	public void toggleRenderSelf()
	{
		renderSelf = !renderSelf;
	}
	
	@Provided
	public int getMenuOptionCount()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public String[] getMenuOptions()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public String[] getMenuTargets()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public int[] getMenuIdentifiers()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public int[] getMenuTypes()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public int[] getMenuActionParams0()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public int[] getMenuActionParams1()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public boolean[] getMenuForceLeftClick()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public void setMenuOptionCount(int i)
	{
		throw new RuntimeException();
	}
	
	@Provided
	public Callbacks getCallbacks()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public RSCollisionData[] getRsCollisionMaps()
	{
		throw new RuntimeException();
	}
	
	@Provided
	public RSDeque getProjectilesDeque()
	{
		throw new RuntimeException();
	}
	
	@Overwrite
	public List getProjectiles()
	{
		List list = new java.util.ArrayList();
		RSNode head = getProjectilesDeque().getHead();
		for (Node node = ((Node) head).getNext();
				node != head;
				node = node.getNext())
		{
			list.add(node);
		}
		return list;
	}
	
	
	@Overwrite
	public RSCollisionData[] getCollisionMaps()
	{
		return getRsCollisionMaps();
	}
	
	@Overwrite
	public void setMenuEntries(MenuEntry[] arrmenuEntry)
	{
		int n2 = 0;
		String[] arrstring = this.getMenuOptions();
		String[] arrstring2 = this.getMenuTargets();
		int[] arrn = this.getMenuIdentifiers();
		int[] arrn2 = this.getMenuTypes();
		int[] arrn3 = this.getMenuActionParams0();
		int[] arrn4 = this.getMenuActionParams1();
		boolean[] arrbl = getMenuForceLeftClick();
		net.runelite.api.MenuEntry[] arrmenuEntry2 = arrmenuEntry;
		int n3 = arrmenuEntry2.length;
		int n4 = 0;
		do
		{
			String string;
			if (n4 >= n3)
			{
				this.setMenuOptionCount(n2);
				oldMenuEntryCount = n2;
				return;
			}
			net.runelite.api.MenuEntry menuEntry = arrmenuEntry2[n4];
			int n5 = menuEntry.getType();
			arrstring[n2] = menuEntry.getOption();
			arrstring2[n2] = menuEntry.getTarget();
			arrn[n2] = menuEntry.getIdentifier();
			arrn2[n2] = n5;
			arrn3[n2] = menuEntry.getParam0();
			arrn4[n2] = menuEntry.getParam1();
			arrbl[n2] = menuEntry.isForceLeftClick();
			++n2;
			++n4;
		}
		while (true);
	}
	
	@Overwrite
	public static void onMenuOptionsChanged(int n2)
	{
		int n3;
		int n4 = oldMenuEntryCount;
		oldMenuEntryCount = n3 = INSTANCE.getMenuOptionCount();
		if (n3 != n4 + 1)
		{
			return;
		}
		net.runelite.api.events.MenuEntryAdded menuEntryAdded =
				new net.runelite.api.events.MenuEntryAdded(INSTANCE.getMenuOptions()[n3 - 1],
						INSTANCE.getMenuTargets()[n3 - 1],
						INSTANCE.getMenuTypes()[n3 - 1],
						INSTANCE.getMenuIdentifiers()[n3 - 1],
						INSTANCE.getMenuActionParams0()[n3 - 1],
						INSTANCE.getMenuActionParams1()[n3 - 1]);
		INSTANCE.getCallbacks().post(menuEntryAdded);
	}
	
	@Overwrite
	public static void copy$processClientError(String s, Throwable e, byte b)
	{
		System.err.println("Error thrown: " + s);
		e.printStackTrace();
	}
	
	@Inject
	public net.runelite.api.MouseRecorder getMouseRecorder()
	{
		return (RSMouseRecorder) _Statics_.mouseRecorder;
	}
	
}

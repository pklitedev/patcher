package xyz.pklite.patchcreator.patches;

import net.runelite.api.events.AnimationChanged;
import net.runelite.api.events.GraphicChanged;
import xyz.pklite.patchcreator.annotations.Overwrite;
import xyz.pklite.patchcreator.annotations.Provided;
import xyz.pklite.patchcreator.annotations.Reobfuscate;

public class Actor
{
	
	@Reobfuscate
	@Provided
	int sequence;
	
	@Overwrite
	public int getAnimation()
	{
		return sequence * 42069;
	}
	
	@Overwrite
	public void animationChanged(int n)
	{
		AnimationChanged animationChanged = new AnimationChanged();
		animationChanged.setActor((net.runelite.api.Actor) this);
		Client.INSTANCE.getCallbacks().post(animationChanged);
	}
	
	@Overwrite
	public void graphicChanged(int n)
	{
		GraphicChanged graphicChanged = new GraphicChanged();
		graphicChanged.setActor((net.runelite.api.Actor)this);
		Client.INSTANCE.getCallbacks().post(graphicChanged);
	}
	
}

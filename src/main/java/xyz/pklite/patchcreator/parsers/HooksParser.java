package xyz.pklite.patchcreator.parsers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.pklite.patchcreator.PatchCreator;
import xyz.pklite.patchcreator.utils.WebUtils;

import java.io.IOException;
import java.util.Map;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

@SuppressWarnings("Duplicates")
public class HooksParser
{
	
	public static final String HOOKS_URL = "https://gitlab.com/pklitedev/bootstrap/raw/master/hooks.json";
	
	public static void run() throws IOException, ParseException
	{
		String jsonContent = WebUtils.getUrlContent(HOOKS_URL);
		JSONParser parser = new JSONParser();
		JSONArray array = (JSONArray) parser.parse(jsonContent);
		
		for (Object object : array)
		{
			JSONObject rootObject = (JSONObject) object;
			String c_obbedName = (String) rootObject.get("name");
			String c_deobbedName = (String) rootObject.get("class");
			PatchCreator.classNames.put(c_deobbedName, c_obbedName);
		}
		
		int failedFields = 0;
		int failedMethods = 0;
		for (Object object : array)
		{
			JSONObject rootObject = (JSONObject) object;
			
			JSONArray fieldArray = (JSONArray) rootObject.get("fields");
			for (Object fieldObj : fieldArray)
			{
				JSONObject field = (JSONObject) fieldObj;
				String f_deobbedName = (String) field.get("field");
				String f_obbedName = (String) field.get("name");
				String f_descriptor = (String) field.get("descriptor");
				String f_owner = (String) field.get("owner");
				long f_decoder = (long) field.getOrDefault("decoder", (long) 1);
				String f_deobbedOwner = null;
				if(f_deobbedName.startsWith("__"))
				{
					continue;
				}
				for (Map.Entry<String, String> entry : PatchCreator.classNames.entrySet())
				{
					if (entry.getValue().equals(f_owner))
					{
						f_deobbedOwner = entry.getKey();
						break;
					}
				}
				if (f_deobbedOwner == null)
				{
					failedFields++;
					//stderr("Failed to find deobbed owner for field %s.%s %s", f_owner, f_obbedName, f_descriptor);
					continue;
				}
				PatchCreator.fieldNames.put(String.format("%s %s %s", f_deobbedOwner, f_deobbedName, f_descriptor),
						String.format("%s %s", f_owner, f_obbedName));
				PatchCreator.fieldDecoders.put(String.format("%s %s", f_deobbedOwner, f_deobbedName),
						f_decoder);
			}
			
			JSONArray methodArray = (JSONArray) rootObject.get("methods");
			for (Object methodObj : methodArray)
			{
				JSONObject method = (JSONObject) methodObj;
				String m_deobbedName = (String) method.get("method");
				String m_owner = (String) method.get("owner");
				String m_deobbedOwner = null;
				String m_obbedName = (String) method.get("name");
				String m_descriptor = (String) method.get("descriptor");
				if(m_deobbedName.startsWith("__"))
				{
					continue;
				}
				for (Map.Entry<String, String> entry : PatchCreator.classNames.entrySet())
				{
					if (entry.getValue().equals(m_owner))
					{
						m_deobbedOwner = entry.getKey();
						break;
					}
				}
				if (m_deobbedOwner == null)
				{
					failedMethods++;
					//stderr("Failed to find deobbed owner for method %s.%s %s", m_owner, m_obbedName, m_descriptor);
					continue;
				}
				PatchCreator.methodNames.put(String.format("%s %s %s", m_deobbedOwner, m_deobbedName, m_descriptor),
						String.format("%s %s", m_owner, m_obbedName));
			}
		}
		stderr("%d unidentified fields", failedFields);
		stderr("%d unidentified methods", failedMethods);
		
	}
}

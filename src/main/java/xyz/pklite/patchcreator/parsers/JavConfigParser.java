package xyz.pklite.patchcreator.parsers;

import xyz.pklite.patchcreator.utils.WebUtils;

import java.io.IOException;

public class JavConfigParser
{
	
	private static final String CONFIG_URL = "http://oldschool.runescape.com/jav_config.ws";
	private static String codebase;
	private static String initial_jar;
	
	public static void run() throws IOException
	{
		String pageText = WebUtils.getUrlContent(CONFIG_URL);
		for (String line : pageText.split("\n"))
		{
			if (line.startsWith("codebase="))
			{
				codebase = line.replace("codebase=", "");
			}
			else if (line.startsWith("initial_jar="))
			{
				initial_jar = line.replace("initial_jar=", "");
			}
		}
	}
	
	public static String getGamepackUrl()
	{
		return codebase + initial_jar;
	}
	
}

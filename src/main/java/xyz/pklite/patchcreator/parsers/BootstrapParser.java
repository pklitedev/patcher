package xyz.pklite.patchcreator.parsers;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import xyz.pklite.patchcreator.utils.WebUtils;

import java.io.IOException;

import static xyz.pklite.patchcreator.PatchCreator.stderr;

public class BootstrapParser
{
	
	private static String clientPatchesUrl, runeliteApiUrl, runescapeApiUrl;
	
	public static void run() throws IOException, ParseException
	{
		String pageText = WebUtils.getUrlContent("http://pklite.xyz/releases/bootstrap.php");
		JSONParser parser = new JSONParser();
		JSONObject rootObject = (JSONObject) parser.parse(pageText);
		JSONArray artifactArray = (JSONArray) rootObject.get("artifacts");
		for (Object object : artifactArray)
		{
			JSONObject artifact = (JSONObject) object;
			String artifactName = (String) artifact.get("name");
			if (artifactName.startsWith("client-patch-"))
			{
				clientPatchesUrl = (String) artifact.get("path");
			}
			else if (artifactName.startsWith("rlapi"))
			{
				runeliteApiUrl = (String) artifact.get("path");
			}
			else if (artifactName.startsWith("rsapi"))
			{
				runescapeApiUrl = (String) artifact.get("path");
			}
		}
		if (clientPatchesUrl == null)
		{
			stderr("Failed to find client patches");
		}
	}
	
	public static String getClientPatchesUrl()
	{
		return clientPatchesUrl;
	}
	
	public static String getRuneliteApiUrl()
	{
		return runeliteApiUrl;
	}
	
	public static String getRunescapeApiUrl()
	{
		return runescapeApiUrl;
	}
}
